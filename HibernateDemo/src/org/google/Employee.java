package org.google;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Emp_tbl",schema="tharak")
public class Employee {

	
	
	@Id
	private Integer eno;
	@Column(name="ename")
	private String name;
	private Integer esal;
	public Integer getEno() {
		return eno;
	}
	public void setEno(Integer eno) {
		this.eno = eno;
	}
	public String getEname() {
		return name;
	}
	public void setEname(String name) {
		this.name = name;
	}
	public Integer getEsal() {
		return esal;
	}
	public void setEsal(Integer esal) {
		this.esal = esal;
	}
	/*public Employee(Integer eno, String name, Integer esal) {
		super();
		this.eno = eno;
		this.name = name;
		this.esal = esal;
	}*/
	public Employee() {
		super();
	}
	
	
}
